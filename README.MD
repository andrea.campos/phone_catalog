In this folder you will find two other folders.

npm i for download node_modules

### rest_API

In the folder called rest_API you will find all the code related to the REST API that I have made for this exercise.
It is an API developed with NodeJS with a single path /api/phones in which 10 elements (mobile) rest, which I later call in my react application.
To raise the API you just have to put the command "npm run serve" and then the local path http://localhost:5500/ appears,
you should only add /api/phone at the end.
For the API I have used Express, Mongoose, Postman and MongoDB. You can see the GET, POST and DELETE methods.
I have also made a model with Schema

### react_app

For the react application I have made two pages, a Home page where you can see the list of the mobile phones that I call from the API,
and a page with a form in which you can add a new simpler mobile than those that rest on the Home.
The form has all the required fields and, in addition, the create button is disabled until all the data is filled out.
The entire application is designed in mobile first and can be viewed for both tablet and desktop.
I have used SCSS for styling. By clicking on the See More Info button, more information about each element is displayed.
Also, a spinner appears until the elements are loaded.
To raise the app you have to type the command "npm run start".
