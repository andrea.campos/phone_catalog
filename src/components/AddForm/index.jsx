import { Link } from "react-router-dom";
import { useState } from "react";

import "./styles.scss";

function AddForm() {
  const [addForm, setAddForm] = useState({
    name: "",
    description: "",
    image: "",
  });

  const [newPhones, setNewPhones] = useState([]);

  const checkIsFormInvalid = (addForm) =>
    Object.values(addForm).filter((value) => !value).length > 0;

  const handleChangeInput = (event) => {
    const newValue = event.target.value;
    const inputName = event.target.name;
    setAddForm({
      ...addForm,
      [inputName]: newValue,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (checkIsFormInvalid(addForm)) {
      return;
    }

    setNewPhones([...newPhones, addForm]);

    // Reset form
    setAddForm({
      name: "",
      description: "",
      image: "",
    });
  };

  const formIsDisabled = checkIsFormInvalid(addForm);

  return (
    <div className="AddForm">
      <h2 className="AddForm__title">Add your own custom Phone</h2>
      <p className="AddForm__disclamer">
        You can add your new phone creation with this form, however,{" "}
        <span style={{ fontWeight: 700 }}>
          it will not be added to the Home catalog
        </span>{" "}
        at the moment.
      </p>
      <form className="AddForm__form" onSubmit={handleSubmit}>
        <fieldset className="AddForm__form__fieldset AddForm__form__fieldset--name">
          <label className="AddForm__form__fieldset__label" htmlFor="name">
            Name
          </label>
          <input
            className="AddForm__form__fieldset__input"
            onChange={handleChangeInput}
            id="name"
            type="text"
            name="name"
            value={addForm.name}
            required
          />
        </fieldset>
        <fieldset className="AddForm__form__fieldset AddForm__form__fieldset--description">
          <label
            className="AddForm__form__fieldset__label"
            htmlFor="description"
          >
            Description
          </label>
          <input
            className="AddForm__form__fieldset__input"
            onChange={handleChangeInput}
            id="description"
            type="text"
            name="description"
            value={addForm.description}
            required
          />
        </fieldset>
        <fieldset className="AddForm__form__fieldset AddForm__form__fieldset--image">
          <label className="AddForm__form__fieldset__label" htmlFor="image">
            Image
          </label>
          <input
            className="AddForm__form__fieldset__input"
            onChange={handleChangeInput}
            id="image"
            type="text"
            name="image"
            value={addForm.image}
            required
          />
        </fieldset>
        <div className="AddForm__button__container">
          <button
            className="AddForm__button AddForm__button--cancel"
            type="button"
          >
            <Link className="AddForm__button__link" to="/">
              Cancel
            </Link>
          </button>
          <button
            className={`AddForm__button ${
              formIsDisabled
                ? "AddForm__button--disabled"
                : "AddForm__button--create"
            }`}
            type="submit"
            disabled={formIsDisabled}
          >
            Create
          </button>
        </div>
      </form>

      {newPhones.length ? (
        <div className="AddForm__gallery">
          <h3 className="AddForm__gallery__title">Your Phone Gallery</h3>
          <div className="AddForm__gallery__wrapper">
            {newPhones.map((newPhone, index) => {
              return (
                <div className="AddForm__gallery__section" key={index}>
                  <h4 className="AddForm__gallery__name">{newPhone.name}</h4>
                  <p className="AddForm__gallery__description">
                    {newPhone.description}
                  </p>
                  <img
                    className="AddForm__gallery__image"
                    src={newPhone.image}
                    alt={newPhone.name}
                  />
                </div>
              );
            })}
          </div>
        </div>
      ) : (
        <span className="AddForm__empty">Your gallery is empty 😔</span>
      )}
    </div>
  );
}

export default AddForm;
