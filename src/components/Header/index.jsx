import { useState } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faHome, faPlus } from "@fortawesome/free-solid-svg-icons";

import "./styles.scss";

function Header() {
  const [showMenu, setShowMenu] = useState(false);

  const handleShowMenu = () => {
    setShowMenu(!showMenu);
  };

  return (
    <div className="Header">
      {/* Header Nav */}
      <nav className="Header__nav__mobile">
          <FontAwesomeIcon className="Header__nav__mobile__bars" onClick={handleShowMenu} icon={faBars} />
        {showMenu ? (
          <ul className="Header__nav__mobile__list">
            <li className="Header__nav__mobile__list__item">
              <Link onClick={handleShowMenu} to="/" className="Header__nav__mobile__link">
                <FontAwesomeIcon icon={faHome} /> Home
              </Link>
            </li>
            <li className="Header__nav__mobile__list__item">
              <Link onClick={handleShowMenu} to="/add-mobile" className="Header__nav__mobile__link">
                <FontAwesomeIcon icon={faPlus} /> Add a mobile
              </Link>
            </li>
          </ul>
        ) : null}
      </nav>

      {/* Desktop Mobile */}
      <nav className="Header__nav__desktop">
        <ul className="Header__nav__desktop__list">
          <li className="Header__nav__desktop__list__item">
            <Link to="/" className="Header__nav__desktop__link">
              <FontAwesomeIcon icon={faHome} /> Home
            </Link>
          </li>
          <li className="Header__nav__desktop__list__item">
            <Link to="/add-mobile" className="Header__nav__desktop__link">
              <FontAwesomeIcon icon={faPlus} /> Add a mobile
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Header;
