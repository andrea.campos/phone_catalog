import spinner from "../../assets/spinner.gif";

import './styles.scss';

function Spinner() {
  return (
    <div className="Spinner">
      <img src={spinner} className="Spinner__image" alt="Loading" />
    </div>
  );
}

export default Spinner;
