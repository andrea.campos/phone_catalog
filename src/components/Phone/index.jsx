import { useState, useEffect } from "react";

import "./styles.scss";

import useFullPageLoader from "../../hooks/useFullPageLoader";

const API_URL = "http://localhost:5500/api/phone";

function Phone() {
  const [phones, setPhones] = useState([]);
  const [isVisible, setIsVisible] = useState(false);

  const [loader, showLoader, hideLoader] = useFullPageLoader();

  useEffect(() => {
    showLoader();
    setTimeout(() => {
      fetch(API_URL)
        .then((res) => res.json())
        .then((apiResponse) => {
          hideLoader();
          setPhones(apiResponse);
        })
        .catch((err) => console.log(err));
    }, 1000);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  function isButtonVisible() {
    setIsVisible(!isVisible);
  }

  return (
    <div className="Phone">
      {phones.map((phone, color) =>
        color ? (
          <li
            className={`Phone__item Phone__item--${phone.color}`}
            key={phone._id}
          >
            <h3
              className={`Phone__item__title Phone__item__title--${phone.color}`}
            >
              {phone.name}
            </h3>
            <h4
              className={`Phone__item__subtitle Phone__item__subtitle--${phone.color}`}
            >
              {phone.manufacturer}
            </h4>
            <div className="Phone__item__image">
              <img
                className="Phone__item__image__product"
                src={phone.image}
                alt={phone.name}
              />
            </div>
            <button
              onClick={() => isButtonVisible(phone._id)}
              className={`Phone__item__button Phone__item__button--${phone.color}`}
            >
              {isVisible ? "Cancel" : "See More Info"}
            </button>
            {isVisible ? (
              <div className={`Phone__detail Phone__detail--${phone.color}`}>
                <p className="Phone__detail__description">
                  {phone.description}
                </p>
                <ul className="Phone__detail__list">
                  <li className="Phone__detail__list__item">
                    <span className="Phone__list__item">
                      Color: {phone.color}
                    </span>
                  </li>
                  <li className="Phone__detail__list__item">
                    <span className="Phone__list__item">
                      Screen: {phone.screen}
                    </span>
                  </li>
                  <li className="Phone__detail__list__item">
                    <span className="Phone__list__item">
                      Processor: {phone.processor}
                    </span>
                  </li>
                  <li className="Phone__detail__list__item">
                    <span className="Phone__list__item">Ram: {phone.ram}</span>
                  </li>
                </ul>
              </div>
            ) : null}
          </li>
        ) : null
      )}
      {loader}
    </div>
  );
}

export default Phone;
