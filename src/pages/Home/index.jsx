import Phone from "../../components/Phone";

import "./styles.scss";

function Home() {

  return (
    <div className="Home">
      <ul className="Home__list">
        <Phone />
      </ul>
    </div>
  );
}

export default Home;
