import "./styles.scss";

import AddForm from "../../components/AddForm";

function AddMobile() {

  return (
    <div className="AddMobile">
      <AddForm />
    </div>
  );
}

export default AddMobile;
