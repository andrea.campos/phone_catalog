import { Route, Switch } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";

import Header from "./components/Header";
import Home from "./pages/Home";
import AddMobile from "./pages/AddMobile";

import "./App.scss";

function App() {
  return (
    <div className="App">
      <Header />
      <h1 className="App__title">
        <FontAwesomeIcon icon={faAppleAlt} className="App__title__icon" />{" "}
        <span>Phone Catalog</span>
      </h1>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/add-mobile" exact>
          <AddMobile/>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
